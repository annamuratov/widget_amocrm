define(['jquery'], function ( $ ) {
    return function () {
        "use strict";

        const Widget = this;
        let WIDGET_CODE;

        const widgetCallbacks = this.callbacks = {};
        widgetCallbacks.bind_actions = widgetCallbacks.onSave = 
        widgetCallbacks.destroy = widgetCallbacks.init = () => true;

        widgetCallbacks.render = function () {
            WIDGET_CODE = Widget.get_settings().widget_code;

            const settings = getSettings();

            const currentUserId = Widget.system().amouser_id;
            let isWidgetUser = false;
            let key;
            for(key in settings){
                if (settings[key] == currentUserId) isWidgetUser = true
                break;
            }

            if(isWidgetUser){
                if (getArea()=='lcard' || getArea()=='ccard'){
                    let notesWrapperTasks = $(".notes-wrapper__tasks");
                    let cardTaskPerformButton = $(".card-task__result-wrapper__inner button");

                    cardTaskPerformButton.attr('disabled', 'disabled');

                    notesWrapperTasks.on('input', 'textarea', function (event) {
                        if ($(this).val().length > 2) {
                            $(this).parent().find('button').removeAttr('disabled');
                            $(this).parent().find('button').removeClass('button-input-disabled');
                        }else{
                            $(this).parent().find('button').attr('disabled', 'disabled');
                            $(this).parent().find('button').addClass('button-input-disabled');
                            event.stopPropagation();
                        }
                    });
                }
            }

            return true;
        };

        widgetCallbacks.settings = function () {
            const settings = getSettings();
            const settingsHiddenInput = document.getElementById( WIDGET_CODE + '_custom' );

            let managers = getAmoUsers();

            var data = Widget.render(
                {data: '<span>Выберите пользователей, к которым должен применяться виджет.</span>' +
                        '<select name="{{name}}" multiple>' +
                        '{% for v in values %}' +
                        '<option value="{{v.id}}" {% if v.id in settings %}selected{% endif %}>{{v.title}}</option>' +
                        '{% endfor %}' +
                        '</select>'},
                {
                    name: 'managers',
                    values: managers,
                    settings: settings
                });

            $('#' + WIDGET_CODE + '_custom_content').append(data);
            $('#' + WIDGET_CODE + '_custom_content').parent().show();

            $('select[name="managers"]').on('change',function() {
                let value = $(this).val();
                let jsonValue = JSON.stringify(value);
                settingsHiddenInput.value = jsonValue;
            });

            return true;
        };

        return this;

        function getSettings() {
            let settings = Widget.get_settings().config;
            if (typeof settings === "string") settings = JSON.parse(settings);
            return settings;
        }

        function getArea () {
            return Widget.system().area;
        }

        function getAmoUsers() {
            return AMOCRM.constant("managers");
        }
    }

})